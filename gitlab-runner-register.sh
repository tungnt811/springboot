registration_token=glrt-UwT3sij7RUGQ3AB9VrHP
url=https://gitlab.com

docker exec -it gitlab-runner \
  gitlab-runner register \
    --non-interactive \
    --registration-token ${registration_token} \
    --locked=false \
	--executor "docker" \
	--docker-image docker:stable \
    --description springboot\
	--docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --url ${url} \
	--docker-privileged = true \
    --tag-list springboot
